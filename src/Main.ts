export function main1(
  input1: string,
  input2: string,
  output: HTMLElement,
): void {
  output.innerText = input1 + ", " + input2;
}
