# Code style intro project

This is a sample project that we'll use in lecture to discuss the topics of formatting and linting. This project has almost the same code as the [TypeScript intro project](https://gitlab.cecs.pdx.edu/cas28/typescript-intro/-/archive/main/typescript-intro-main.zip), but with the addition of a formatter ([Prettier](https://prettier.io/)) and a linter ([ESLint](https://eslint.org/)) in the project setup.

Download the source code of this project from <https://gitlab.cecs.pdx.edu/cas28/style-intro/-/archive/main/style-intro-main.zip>

You'll need to go through the same setup instructions as in the TypeScript intro project, including running the `npm i` command once in the root folder for this project.


## Formatting and linting

To run the formatter or linter from the terminal, run `npm run format` or `npm run lint` from the project root folder. In VSCode, choose the "format" or "lint" option from the "Run Build Task" menu.

To run the formatter and linter automatically in VSCode whenever your code changes, install the "Prettier" and "ESLint" extensions. You may need to restart VSCode in order for the extensions to take effect.
